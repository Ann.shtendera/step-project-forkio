"use strict"

const dropDownButton = document.querySelector('.header__drop-down');
const headerNav = document.querySelector('.header__nav-list');
const burgerMenu = document.querySelectorAll('.header__burger-menu');
const burgerMenuClick = document.querySelector('.header__btn--press');

const pressBurgerMenu = () => {
    headerNav.classList.toggle('dropdown');
    burgerMenu.forEach(el => {
        el.classList.toggle('js-active');   
    })
}

const closeNavMenu = () => {
    headerNav.classList.remove('dropdown');
    burgerMenu.forEach(el => {
        el.classList.toggle('js-active');   
    }) 
}

document.body.addEventListener('click', e => {
if ((e.target.parentElement === dropDownButton) || (e.target === dropDownButton)) {
    pressBurgerMenu(); 
} else if ((!e.target.closest('.dropdown')) && (headerNav.classList.contains('dropdown'))) {
    closeNavMenu();   
} 
})